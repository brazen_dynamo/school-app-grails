package schoolapp

class TeacherController {

    def overview() {
    	def teachers = Teacher.list()
    	[teachers: teachers]
    }
}
