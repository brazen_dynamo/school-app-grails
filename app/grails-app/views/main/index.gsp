<%@page import = "schoolapp.*"%>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>Main Page</title>
	</head>
	<body>
		Hello! This is a beginner's first serious trivial project in Grails!<br/><br/>

		Click any of the links to check out a simple school database.

		<a href="../teacher/overview">Teachers</a>
		<a href="../student/overview">Students</a>
		<a href="../subject/overview">Subjects</a>
		<a href="../section/overview">Sections</a>
	</body>
</html>