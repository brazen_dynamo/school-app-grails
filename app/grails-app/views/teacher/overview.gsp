<%@page import = "schoolapp.*"%>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>Teacher Overview</title>
	</head>
	<body>
		<tr>
			<g:each in="${teachers.properties.findAll(it.key != 'class')}" var="property">
				<th>${property}</th>
			</g:each>
		</tr>
		<g:each in="${teachers}" var="teacher" status="i">
			<tr>
				<td>${i + 1}</td>
				<td>${teacher.firstName}</td>
				<td>${teacher.lastName}</td>
				<td>${teacher.age}</td>
				<td>${teacher.specialty}</td>
			</tr>
		</g:each>
		<a href="../main">Back to main page</a>
	</body>
</html>